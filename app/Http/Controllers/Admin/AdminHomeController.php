<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
class AdminHomeController extends Controller
{
public function index()
{
return view('espace admin.index');
}
public function listeUser()
{
return view('espace admin.listeusers');
}
public function addUser()
{
return view('espace admin.adduser');
}
public function validerEntreprise()
{
return view('espace admin.validation');
}
public function confirmerEntre()
{
return view('espace admin.valider');
}
public function modifierEntreprise()
{
return view('espace admin.modifier');
}
public function Entreprisemodifier()
{
return view('espace admin.entreprismodifier');
}
public function detailEntreprise()
{
return view('espace admin.detailentreprise');
}
}

