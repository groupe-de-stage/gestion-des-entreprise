<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entreprise extends Model
{
    use HasFactory;
    protected $fillable = [
        'Taxe professionnelle',
        'Raison sociale',
        'Abréviation',
        'Adresse',
        'Tél Fixe',
        'Email',
        'RC',
        'ICE',
        'img',
        'dateCreation',
        'Capital',
        'etrenger',
        'nationalité',
        'franchises',
        'labels_certification',
        
    ];
}
