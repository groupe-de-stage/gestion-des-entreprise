<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Particuliére extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'fax',
        'email',
        'tele',
        
    ];
}
