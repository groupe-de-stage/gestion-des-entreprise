<?php



use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Admin\AdminHomeController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|

 */

Route::get('/', 'App\Http\Controllers\AccueilPage@index')->name('accuiel');

Route::get('/search', 'App\Http\Controllers\SearchController@index')->name('search');
Auth::routes();

Route::get('/admin', 'App\Http\Controllers\Admin\AdminHomeController@index')->name('admin.index');
Route::get('/admin/listeusers', 'App\Http\Controllers\Admin\AdminHomeController@listeUser')->name('admin.listeuser');
Route::get('/admin/adduser', 'App\Http\Controllers\Admin\AdminHomeController@addUser')->name('admin.adduser');
Route::get('/admin/validation', 'App\Http\Controllers\Admin\AdminHomeController@validerEntreprise')->name('admin.validation');
Route::get('/admin/confirmer', 'App\Http\Controllers\Admin\AdminHomeController@confirmerEntre')->name('admin.confirmer');
Route::get('/admin/modifier', 'App\Http\Controllers\Admin\AdminHomeController@modifierEntreprise')->name('admin.modifier');
Route::get('/admin/entraprismodifier', 'App\Http\Controllers\Admin\AdminHomeController@Entreprisemodifier')->name('admin.entraprismodifier');
Route::get('/admin/detailentraprise', 'App\Http\Controllers\Admin\AdminHomeController@detailEntreprise')->name('admin.detailentraprise');

Route::get('/trouve', 'App\Http\Controllers\EspaceEntropriseController@index')->name('form.entreprise');



Route::view('/utilisateur', 'utilisateur.valEspace')->name('user.index');
Route::view('/utilisateur/view', 'utilisateur.viewall')->name('user.viewall');
Route::view('/utilisateur/edit', 'utilisateur.modifier')->name('user.edit');
Route::view('/userlogin', 'utilisateur.login')->name('user.login');



