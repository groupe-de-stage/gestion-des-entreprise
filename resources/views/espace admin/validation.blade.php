@extends('espace admin.layoute.adminpanel')
@section('content')
<div class="text-center">
  <h1>Validation du Request</h1>
</div><br>
<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">R-Social</th>
        <th scope="col">Activité</th>
        <th scope="col">Téléphone</th>
        <th scope="col">Email</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Copacoration</td>
        <td>Transport national </td>
        <td>06xxxxxxxxx </td>
        <td>xxxxxxxx@gmail.com</td>
        <td>
          <button class="btn " ><a href="{{route('admin.confirmer')}}" class=""><i class="fa-regular fa-square-plus"></i></button>
          <button class="btn "><i class="fa-solid fa-trash"></i></button>
          {{-- <button class="btn "><i class="fa-solid fa-pen-to-square"></i></button> --}}
            
        </td>
      </tr>
      <tr>
        <td>Copacoration</td>
        <td>Transport national</td>
        <td>06xxxxxxxxx</td>
        <td>xxxxxxxx@gmail.com</td>
        <td>
          <button class="btn " ><a href="{{route('admin.confirmer')}}" class=""><i class="fa-regular fa-square-plus"></i></button>
          <button class="btn "><i class="fa-solid fa-trash"></i></button>
          {{-- <button class="btn "><i class="fa-solid fa-pen-to-square"></i></button> --}}
            
        </td>
      </tr>
      <tr>
        <td>Copacoration</td>
        <td>Transport national</td>
        <td>06xxxxxxxxx</td>
        <td>xxxxxxxx@gmail.com</td>
        <td>
            <button class="btn " ><a href="{{route('admin.confirmer')}}" class=""><i class="fa-regular fa-square-plus"></i></button>
            <button class="btn"></a><i class="fa-solid fa-trash"></i></button>
            {{-- <button class="btn "><i class="fa-solid fa-pen-to-square" ></i></button> --}}
            
        </td>
      </tr>
      <tr>
        <td>Copacoration</td>
        <td>Transport national</td>
        <td>06xxxxxxxxx</td>
        <td>xxxxxxxx@gmail.com</td>
        <td>
          <button class="btn " ><a href="{{route('admin.confirmer')}}" class=""><i class="fa-regular fa-square-plus"></i></button>
            <button class="btn "><i class="fa-solid fa-trash"></i></button>
            {{-- <button class="btn "><i class="fa-solid fa-pen-to-square"></i></button> --}}
            
        </td>
      </tr>
    </tbody>
  </table>
  
@endsection