@extends('espace admin.layoute.adminpanel')
@section('content')
<div class="text-center">
    <h1>Liste des entreprises</h1>
  </div><br>
<div class="navbar navbar-light bg-light text-center "> 
    {{-- <p>Filter by :</p> --}}
    <form class="form-inline text-center">
      <button class="btn btn-outline-success text-light btn-list " type="button">recherche par R.social</button>
      <button class="btn btn-outline-success text-light btn-list " type="button">recherche par activité</button>
      <button class="btn btn-outline-success text-light btn-list" type="button">recherche par ville</button>
      <button class="btn btn-outline-success text-light btn-list" type="button">recherche par ICE</button>
    </form>
  </div>

<div class="navbar navbar-light bg-light  ">
    <form class="form-inline">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <input class="form-control mr-sm-2" type="search" placeholder="Où (Agadir)" aria-label="Search">
      <button class="btn btn-s my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div><br>
  
  <table class="table table-striped">
      <thead>
        <tr>
          <th scope="col">R-Social</th>
          <th scope="col">Activité</th>
          <th scope="col">Téléphone</th>
          <th scope="col">Email</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Copacoration</td>
          <td>Transport national </td>
          <td>06xxxxxxxxx </td>
          <td>xxxxxxxx@gmail.com</td>
          <td>
            <button class="btn " ><a href="{{route('admin.detailentraprise')}}" class=""><i class="fa-regular fa-square-plus"></i></button>
            <button class="btn "><i class="fa-solid fa-trash"></i></button>
            <button class="btn "><a href="{{route('admin.entraprismodifier')}}" class=""><i class="fa-solid fa-pen-to-square"></i></button>
              
          </td>
        </tr>
        <tr>
          <td>Copacoration</td>
          <td>Transport national</td>
          <td>06xxxxxxxxx</td>
          <td>xxxxxxxx@gmail.com</td>
          <td>
            <button class="btn " ><a href="{{route('admin.detailentraprise')}}" class=""><i class="fa-regular fa-square-plus"></i></button>
                <button class="btn "><i class="fa-solid fa-trash"></i></button>
            <button class="btn "><a href="{{route('admin.entraprismodifier')}}" class=""><i class="fa-solid fa-pen-to-square"></i></button>
              
          </td>
        </tr>
        <tr>
          <td>Copacoration</td>
          <td>Transport national</td>
          <td>06xxxxxxxxx</td>
          <td>xxxxxxxx@gmail.com</td>
          <td>
            <button class="btn " ><a href="{{route('admin.detailentraprise')}}" class=""><i class="fa-regular fa-square-plus"></i></button>
                <button class="btn"></a><i class="fa-solid fa-trash"></i></button>
              <button class="btn "><a href="{{route('admin.entraprismodifier')}}" class=""><i class="fa-solid fa-pen-to-square"></i></button>
              
          </td>
        </tr>
        <tr>
          <td>Copacoration</td>
          <td>Transport national</td>
          <td>06xxxxxxxxx</td>
          <td>xxxxxxxx@gmail.com</td>
          <td>
            <button class="btn " ><a href="{{route('admin.detailentraprise')}}" class=""><i class="fa-regular fa-square-plus"></i></button>
                <button class="btn "><i class="fa-solid fa-trash"></i></button>
              <button class="btn "><a href="{{route('admin.entraprismodifier')}}" class=""><i class="fa-solid fa-pen-to-square"></i></button>
              
          </td>
        </tr>
      </tbody>
    </table>
@endsection

