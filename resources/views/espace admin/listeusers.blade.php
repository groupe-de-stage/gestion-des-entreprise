@extends('espace admin.layoute.adminpanel')
@section('content')
<div class="text-center">
  <h1>list des utilisateurs</h1>
  
</div><br>

<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">id</th>
        <th scope="col">Nom</th>
        <th scope="col">Prénome</th>
        <th scope="col">Email</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td>Alaoui</td>
        <td>Salim </td>
        <td>salim.alaoui@gmail.com</td>
        <td><button class="btn "><i class="fa-solid fa-trash"></i></button></td>
      </tr>
      <tr>
        <td>2</td>
        <td>Benani</td>
        <td>Salah</td>
        <td>salah.benani@gmail.com</td>
        <td><button class="btn "><i class="fa-solid fa-trash"></i></button></td>
      </tr>
      <tr>
        <td>3</td>
        <td>tohami</td>
        <td>Layla</td>
        <td>Layla.tohami@gmail.com</td>
        <td><button class="btn "><i class="fa-solid fa-trash"></i></button></td>
      </tr>
      <tr>
        <td>4</td>
        <td>Regragi</td>
        <td>Reda</td>
        <td>Reda.Regragi@gmail.com</td>
        <td><button class="btn "><i class="fa-solid fa-trash"></i></button></td>
      </tr>
    </tbody>
  </table>
@endsection