<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link href="{{ asset('/adminpanel.css') }}" rel="stylesheet" />
<link rel="icon" type="image/x-icon" href="{{ asset('img/img2.png') }}">
<title>Admin-CCIS</title>
</head>
<body>
<div class="row g-0">
<!-- sidebar -->
<div class="p-3 col fixed text-white d-flex flex-column min-vh-100 sedb">
<a href="" class="text-white text-decoration-none">
<span class="fs-4">Admin Panel</span>
</a>
<hr />
<ul class="nav flex-column">
<li><a href="{{route('admin.index')}}" class="nav-link text-white">Liste des entreprises</a></li>
<li><a href="{{route('admin.validation')}}" class="nav-link text-white">Validation requests</a></li>
<li><a href="{{route('admin.listeuser')}}" class="nav-link text-white">liste des utilisateurs</a></li>
<li><a href="{{route('admin.adduser')}}" class="nav-link text-white">Ajouter un utilisateurs</a></li><br>
<li>
<a href="{{route('accuiel')}}" class="mt-2 btn  text-white btnb"> Back to Home page</a>
</li>
</ul>
</div>
<!-- sidebar -->
<div class="col content-grey">
<nav class="p-3 shadow text-end">
{{-- <span class="profile-font">Admin</span>
<img class="img-profile rounded-circle" src="{{ asset('/img/undraw_profile.svg') }}"> --}}
</nav>
<div class="g-0 m-5">
@yield('content')
</div>
</div>
</div>
<!-- footer -->
<div class="copyright py-4 text-center text-white mt-auto">
<div class="container">
<small>
Copyright - <a class="text-reset fw-bold text-decoration-none" target="_blank"
href="https://twitter.com/danielgarax">
CCIS 2023
</a>
</small>
</div>
</div>
<!-- footer -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous">
</script>
</body>
</html>