<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="icon" type="image/x-icon" href="{{ asset('img/img2.png') }}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    
    <link rel="stylesheet" href="{{ asset('/app.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css"
        integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<style>
    .important{
        border-color: #41B3DD ;
        
        border-width: 5px
    }
</style>

    <title>CCIS</title>
</head>

<body>

    <header class="bigNav ">
        <div class="container  " >
            <nav class="navbar">
                <div class="logo">
                    <a href="{{route('accuiel')}}">
                        <img loading="lazy" class="logo-image" src="img/img1.png" alt="logo telecontact">
                    </a>
                    <div class="sandwitch-icon">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </div>
                </div>
                
                <ul class="links ">
                    <!--<li>
                        <a class="bar" style="text-decoration: underline" href="/consulter-demande-devis-all/page/1">
                            <span class="list" data-bs-toggle="tooltip" data-bs-placement="bottom" title="publier ou consulter une demande de devis">
                                <img src="/trouver/media/images/demande-devis-icon.png" alt="icon demande devis"/>
                                Demande de devis
                            </span>
                        </a>     
                    </li>-->
                    <li><a class="bar text-dark" style="text-decoration: none"  href="{{route('accuiel')}}">accuiel</a></li>
                    <li><a class="bar text-dark" style="text-decoration: none"  href="{{route('form.entreprise')}}">espace entreprise</a></li>
                    <li><a class="bar text-dark" style="text-decoration: none"  href="{{route('user.index')}}">espace utilisateur</a></li>
                    <!--<li><a class="bar" href=""><img src="/trouver/media/images/bullhorn.png" /> Demande de devis</a></li>-->
                   
                                        <li class="active "><a class="text-white" href="{{route('user.login')}}" data-bs-toggle="modal" style="background-color: #5166A0; border-radius:10px" data-bs-target="#authentication-homepage">Connexion</a></li>
                        
                </ul>    
            </nav>
        </div>
    </header>
        <div class="">
        @yield('content')
    </div>


    <!-- footer -->
    <footer class=" text-center text-dark" style="background-color:  #D7FCFE">
        <!-- Grid container -->
        <div class="container p-4 pb-0">
            <!-- Section: Social media -->
            <section class="mb-4">
                <!-- Facebook -->
                <a class="btn text-white btn-floating m-1" style="background-color: #3b5998;" href="#!" role="button"><i
                        class="fab fa-facebook-f"></i></a>

                <!-- Twitter -->
                <a class="btn text-white btn-floating m-1" style="background-color: #55acee;" href="#!" role="button"><i
                        class="fab fa-twitter"></i></a>

                <!-- Google -->
                <a class="btn text-white btn-floating m-1" style="background-color: #dd4b39;" href="#!" role="button"><i
                        class="fab fa-google"></i></a>

                <!-- Instagram -->
                <a class="btn text-white btn-floating m-1" style="background-color: #ac2bac;" href="#!" role="button"><i
                        class="fab fa-instagram"></i></a>

                <!-- Linkedin -->
                <a class="btn text-white btn-floating m-1" style="background-color: #0082ca;" href="#!" role="button"><i
                        class="fab fa-linkedin-in"></i></a>
                <!-- Github -->
                <a class="btn text-white btn-floating m-1" style="background-color: #333333;" href="#!" role="button"><i
                        class="fab fa-github"></i></a>
            </section>
            <!-- Section: Social media -->
        </div>
        <!-- Grid container -->

        <!-- Copyright -->
        <div class="text-center p-3" style="background-color: #41B3DD;">
            © 2023 Copyright:
            <a class="text-dark" href="http://www.ccis-agadir.com">CCIS.com</a>
        </div>
        <!-- Copyright -->
    </footer>
    <!-- footer -->

    </div>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous">
    </script>
</body>

</html>
