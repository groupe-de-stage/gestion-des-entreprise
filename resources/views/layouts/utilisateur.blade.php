<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
        crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link href="{{ asset('/css/utilisateur.css') }}" rel="stylesheet" />
    <title>@yield('title', 'Utilisateur - CCIS')</title>
</head>

<body>
    <div class="row g-0">
        <!-- sidebar -->
        <div class="p-3 col fixed text-white btnb">
            <a href="{{ route('user.index') }}" class="text-white text-decoration-none">
                <span class="fs-4">Utilisateur Panel</span>
            </a>
            <hr />
            <ul class="nav flex-column">
                <li><a href="{{ route('user.index') }}" class="nav-link text-white">- Verification Espace -</a></li>
                <li>
                    <a href="#" class="mt-2 btn bg-danger text-white">Déconnexion</a>
                </li>
            </ul>
        </div>
        <!-- sidebar -->
        <div class="col content-grey">
            <nav class="p-3 shadow text-end">
                <!-- <span class="profile-font">Utilisateur</span> -->
            </nav>
            <div class="g-0 m-5">
                @yield('content')
            </div>
        </div>
    </div>
    <!-- footer -->
    <div class="copyright py-4 text-center text-white btnb">
        <div class="container ">
            <small>
                © 2023 Copyright:
                <a class="text-white" href="http://www.ccis-agadir.com">CCIS.com</a>
                </a>
            </small>
        </div>
    </div>
    <!-- footer -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous">
    </script>
</body>

</html>
