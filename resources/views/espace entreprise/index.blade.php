@extends('layouts.app')
@section('content')
<div>
    <div class="col">
        <div class="row g-0">
            <div class="col-xl-6 d-none d-xl-block">
            </div>
            <div class="col-xl-0">
                <div class="card-body p-md-3 text-black">
                    <form action="">
                        <div class="hihi">
                            <h2 class="text-center mt-2 " style="color: rgb(45, 104, 104)">FICHE D'IDENTIFICATION DE <strong>L'ENTREPRISE</strong></h2>

                            <div class="row mt-4">
                                <div class="col-md-6 mb-2">
                                    <div class="form-outline">
                                        <label class="form-label" for="form3Example1m">Raison Sociale  <span class="text-danger">*(obligatoire)</span> </label>
                                        <input type="text" id="form3Example1m" placeholder="Raison Sociale" name="Raison Sociale" class="form-control form-control-lg" />

                                    </div>
                                </div>
                                <div class="col-md-6 mb-2">
                                    <div class="form-outline">
                                        <label class="form-label" for="form3Example1n">Abreviation</label>
                                        <input type="text" id="form3Example1n" name="Abreviation" class="form-control form-control-lg" />

                                    </div>
                                </div>
                            </div>



                            <div class="row">
                                <div class="col-md-6 mb-2">

                                    <label class="form-label" for="form3Example1n">Details activite  </label>
                                    <textarea type="text" id="form3Example1m" name="Details activite" id="exampleInputEmail1"
                                        class="form-control form-control-lg"></textarea>
                                </div>
                                <div class="col-md-6 mb-2">
                                    <label class="form-label" for="form3Example1n">Activite Principale </label>
                                    <select class="form-select" name="Activite Principale" aria-label="Default select example">
                                        <option selected>Open this select menu</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-2">

                                    <label class="form-label" for="form3Example1n">Adresse <span class="text-danger">*(obligatoire)</span> </label>
                                    <textarea type="text" id="form3Example1m" name="Adresse " id="exampleInputEmail1"
                                        class="form-control form-control-lg"></textarea>
                                </div>
                                <div class="col-md-6 mb-2">
                                    <label class="form-label" for="form3Example1n">Forme Juridique </label>
                                    <select class="form-select" name="Forme Juridique" aria-label="Default select example">
                                        <option selected>Open this select menu</option>
                                        <option  value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                            </div>
                           
                            <div class="row">
                                <div class="col-md-4 mb-2">
                                    <div class="form-outline">
                                        <label class="form-label" for="form3Example1m">Email <span class="text-danger">*(obligatoire)</span> </label>
                                        <input type="text" id="form3Example1m" name="Email" class="form-control form-control-lg" />
    
                                    </div>
                                </div>
                                <div class="col-md-4 mb-2">
                                    
                                    <div class="form-outline">
                                        <label class="form-label" for="form3Example1n">Téléphone <span class="text-danger">*(obligatoire)</span> </label>
                                        <input type="text" id="form3Example1n" name="Téléphone" class="form-control form-control-lg" />

                                    </div>
                                </div>
                                <div class="col-md-4 mb-2">
                                    <div class="form-outline">
                                        <label class="form-label" for="form3Example1n"> Faxe <span class="text-danger">*(obligatoire)</span></label>
                                        <input type="text" id="form3Example1n" name="Faxe" class="form-control form-control-lg" />

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 mb-2">

                                    <label class="form-label" for="form3Example1n">Province <span class="text-danger">*(obligatoire)</span> </label>
                                    <select class="form-select" name="Province" aria-label="Default select example">
                                        <option selected>Open this select menu</option>
                                        <option value="1">Agadir Ida-Outanane</option>
                                        <option value="2">'Inezgane-Aït Melloul</option>
                                        <option value="3">Taroudant</option>
                                        <option value="3">Tiznit</option>
                                    </select>
                                </div>
                                <div class="col-md-4 mb-2">
                                    <label class="form-label" for="form3Example1n">Ville <span class="text-danger">*(obligatoire)</span> </label>
                                    <select class="form-select" aria-label="Default select example" name="Ville">
                                        <option selected>Open this select menu</option>
                                        <option value="1">Agadir</option>
                                        <option value="2">Inzegane</option>
                                        <option value="3">Ait Melloul</option>
                                    </select>
                                </div>
                                <div class="col-md-4 mb-2">
                                    <label class="form-label" for="form3Example1n">Zone  </label>
                                    <select class="form-select" aria-label="Default select example" name="Zone">
                                        <option selected>Open this select menu</option>
                                        <option value="1">industrielle</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 mt-4 text-center"> <button type="button"
                                    class="btn btn-success b1">Suivant</button></div>
                        </div>
                </div>
            </div>

            <div class="p-md-1 ">
                <div class="row b2" style="display: none">

                    <div class="mx-auto col-10 col-md-8 col-lg-6">
                        <h2 class="text-center " style="color: rgb(45, 104, 104)">DIRIGEANT OU PERSONNE DE CONTACT</h2>
                        <div class="form-group">
                            <label class="form-label" for="form3Example1m">Name </label>
                            <input type="text" id="form3Example1m" name="Name" class="form-control form-control-lg" />
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="form3Example1n">Function  </label>
                            <select class="form-select" name="Function" aria-label="Default select example">
                                <option selected>Open this select menu</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="form3Example1m">Email  </label>
                            <input type="text" id="form3Example1m" name="Email" class="form-control form-control-lg" />
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="form3Example1m">GSM  </label>
                            <input type="text" id="form3Example1m" name="GSM" class="form-control form-control-lg" />
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="form3Example1m">Facebook  </label>
                            <input type="text" id="form3Example1m" name="Facebook" class="form-control form-control-lg" />
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="form3Example1m">Linkedin  </label>
                            <input type="text" id="form3Example1m" name="Linkedin" class="form-control form-control-lg" />
                        </div>

                        <div class="col-md-12 mt-4 text-center"> <button type="button"
                                class="btn btn-danger px-4 mx-5 btn1">precedent</button>
                            <button type="button" class="btn btn-success px-5  t1">Suivant</button>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>


        <div class=" p-md-0  ">

            <div class="col mt-2 t2" id="t2" style="display: none">
                <h2 class="text-center " style="color: rgb(45, 104, 104)">INFORMATIONS COMPLEMENTAIRES</h2>
                <div class="col">
                    <div class="row g-0">
                        <div class="col-xl-6 d-none d-xl-block">
                        </div>
                        <div class="col-xl-0">
                            <div class="card-body p-md-3 text-black">

                                <div class="row">
                                    <div class="col-md-6 mb-3">
                                        <div class="form-outline">
                                            <label class="form-label" for="form3Example1m">Taxe Professionnelle 
                                            </label>
                                            <input type="text" id="form3Example1m" name="Taxe Professionnelle"
                                                class="form-control form-control-lg" />

                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <div class="form-outline">
                                            <label class="form-label" for="form3Example1n">RC </label>
                                            <input type="text" id="form3Example1n" name="RC"
                                                class="form-control form-control-lg" />

                                        </div>
                                    </div>
                                </div>




                                <div class="row">
                                    <div class="col-md-6 mb-3">
                                        <div class="form-outline">
                                            <label class="form-label" for="form3Example1m">ICE </label>
                                            <input type="text" id="form3Example1m" name="ICE"
                                                class="form-control form-control-lg" />

                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <div class="form-outline">
                                            <label class="form-label" for="form3Example1n">Date de creation </label>
                                            <input type="date" id="form3Example1n" name="Date de creation"
                                                class="form-control form-control-lg" />

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 mb-4">

                                        <label class="form-label" for="form3Example1n">Capital en DHS  </label>
                                        <input type="text" id="form3Example1n" name="Capital en DHS" class="form-control form-control-lg" />
                                    </div>
                                    <div class="col-md-4 mb-4">
                                        <label class="form-label" for="form3Example1n">%Etranger  </label>
                                        <input type="text" id="form3Example1n" name="Etranger" class="form-control form-control-lg" />
                                    </div>
                                    <div class="col-md-4 mb-4">
                                        <label class="form-label" for="form3Example1n">Nationalite  </label>
                                        <input type="text" id="form3Example1n" name="Nationalite" class="form-control form-control-lg" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 mb-4  ">

                                        <label class="form-label h6" for="form3Example1n">Chiffre d'affaires : </label>
                                        <div class="">
                                        <input type="radio" name="Chiffre d'affaires" id="" value="3 Millions Dhs" class="form-check-input mt-1  mx-2">3 Millions Dhs
                                        <input type="radio" name="Chiffre d'affaires" id="" value="3 à 50 Millions Dhs" class="form-check-input mt-1 mx-2">3 à 50 Millions Dhs
                                        <input type="radio" name="Chiffre d'affaires" id="" value="plus 50 Millions Dhs" class="form-check-input mt-1 mx-2">plus 50 Millions Dhs
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-4   ">
                                        <div class="">
                                        <label class="form-label h6" for="form3Example1n">Effectif :  </label><br>
                                        <input type="radio" name="Effectif" id="" value="à 10" class="form-check-input mt-1 mx-2">à 10
                                        <input type="radio" name="Effectif" id="" value="10 à 50" class="form-check-input mt-1 mx-2">10 à 50
                                        <input type="radio" name="Effectif" id="" value="51 à 200" class="form-check-input mt-1 mx-2"> 51 à 200
                                        <input type="radio" name="Effectif" id="" value="> 200" class="form-check-input mt-1 mx-2">> 200
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 mb-3">
                                        <div class="form-outline">
                                            <label class="form-label" for="form3Example1m">Pays d'exportation  </label>
                                            <input type="text" id="form3Example1m" name="Pays d'exportation"
                                                class="form-control form-control-lg" />

                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <div class="form-outline">
                                            <label class="form-label" for="form3Example1n">Pays d'importation </label>
                                            <input type="text" id="form3Example1n" name="Pays d'importation"
                                                class="form-control form-control-lg" />

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 mb-3">
                                        <div class="form-outline">
                                            <label class="form-label" for="form3Example1m">Marques representees 
                                            </label>
                                            <input type="text" id="form3Example1m" name="Marques representees"
                                                class="form-control form-control-lg" />

                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <div class="form-outline">
                                            <label class="form-label" for="form3Example1n">Franchies </label>
                                            <input type="text" id="form3Example1n" name="Franchies"
                                                class="form-control form-control-lg" />

                                        </div>
                                    </div>
                                </div>
                                


                                    <div class="row">
                                        <div class="col-md-4 mb-4">
    
                                            <label class="form-label" for="form3Example1n">Labels/Certifications  </label>
                                            <input type="text" id="form3Example1n" name="Labels/Certifications" class="form-control form-control-lg" />
                                        </div>
                                        <div class="col-md-4 mb-4">
                                            <label class="form-label" for="form3Example1n">Logo  </label>
                                            <input type="file" id="form3Example1n" name="Logo" class="form-control form-control-lg" />
                                        </div>
                                        <div class="col-md-4 mb-4">
                                            <label class="form-label" for="form3Example1n">Flyer  </label>
                                            <input type="file" id="form3Example1n" name="Flyer" class="form-control form-control-lg" />
                                        </div>
                                    </div>
                                </div>
                                <div class="m-4 p-3">
                                    <h5>Etes-vous intérressé par une insertion publicitaire ?</h5>
                                <label for="" class="h5 m-3">Oui
                                <input type="radio" name="pub" value="oui" id=""  >
                                </label>
                                <label for="" class="h5 m-3">Non
                                <input type="radio" name="pub" value="non" id="">
                                </label>
                                </div>

                                <div class="col-md-12 text-center"> <button type="button" id="btn2"
                                        class="btn btn-danger px-4 mx-4 btn2">precedent</button>
                                    <input type="submit" class="btn btn-success px-5" value="Save" name="Save" />
                                </div>

                            </div>
                        </div>


                        <script>
                        $(document).ready(function() {
                            //Dès qu'on clique sur #b1, on applique hide() au titre
                            $(".b1").click(function() {
                                $(".hihi").hide();
                                $(".b2").show();
                            });
                            $(".t1").click(function() {
                                $(".hihi").hide();
                                $(".b2").hide();
                                $(".t2").show();
                            });
                            $(".btn1").click(function() {
                                $(".t2").hide();
                                $(".b2").hide();
                                $(".hihi").show();
                            });
                            $(".btn2").click(function() {
                                $(".t2").hide();
                                $(".b2").show();
                                $(".hihi").hide();
                            });
                            $(".btn3").click(function() {
                                $(".t2").show();
                                $(".b2").hide();
                                $(".hihi").hide();
                            });

                            //Dès qu'on clique sur #b1, on applique show() au titre

                        });
                        </script>
                        @endsection
