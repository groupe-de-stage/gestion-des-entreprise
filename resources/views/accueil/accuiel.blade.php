@extends('layouts.app')
@section('content')

<div class="main-header my-5 mx-5  " style="border-radius: 25px ; background-color: #41B3DD">
  <div class="container">
  
      <!--<div class="headline-main-header-habillage">
          <span>Bonne année</span>
          <img src="/trouver/media/images/img-2023.png" alt="habillage icon" />
          <span>Meilleurs voeux</span>
      </div>-->

      <div class="headline-main-header text-white ">
          <h1>La référence de toutes vos recherches de professionnels au <strong>Souss Massa</strong> 
          </h1>
      </div>

      <div class="main-header-box-search   " style="border-radius: 15px " >
  <ul class="main-header-box-search-links ">
      <li><a class="mx-3"  href="/">Recherche par ville</a></li>
      
      <li><a class="mx-3" href="">Recherche par r.social</a></li>
      <li><a class="mx-3" class="active" href="">Recherche par Marque</a></li>
      <li><a class="mx-3" href="">Recherche par ICE</a></li>
  </ul>

  <form method="GET" action="/trouver/index.php" class="main-header-box-search-moteur">
      <div class="row">
        <div class="col-md-12 col-lg-7">
          <i class="fa-solid fa-magnifying-glass"></i>
          <input type="text" autocomplete="off" placeholder="Qui, quoi ? ( activité, produit, service, nom d'entreprise ou professionnel..)" name="string" class="form-control" required="required">
          <ul class="autocomplete">
          </ul>
      </div>
          <div class="col-md-12 col-lg-3">
            <i class="fa-solid fa-location-dot"></i>
            <input type="text" autocomplete="off" id="input-ou" name="ou" placeholder="Où (Casablanca)" class="form-control" required="required">
            <ul class="autocomplete">
            </ul>
        </div>
          <div class="col-md-2">
              <button onclick="pageTracker._trackEvent('Trouver', 'Trouver', 'accueil', 1);" class="btn text-white " style="background-color: #5166A0" type="submit">
                  <i class="fa-solid fa-magnifying-glass "></i>
                  <span>search<span>
              </span></span></button>
          </div>
      </div>
  </form>
</div>
  </div>
</div>
    <div style="" class="aga agaWeb container-fluid" >@yield('name')
      <div class=" row find  mx-3 container-fluid" style="background-color: #E8E9E9">
        <div class=" col-md-2">
            <img src="img/img8.png" class="rounded img" alt="Cinque Terre">
        </div>
        <div class="col-md-10">
            <h2>agaWeb</h2>
            <h5>Hadj Lahbib cité les Amicales - Agadir - Maroc</h5>
            <p>Agence de communication et de publicité, design graphique,adeaux d'entreprises, publicité par objet.</p>
           <div class="px-5">
            <button class="btn text-white px-5 mx-5 msg" style="background-color: #5166A0">send message</button>
            <button class="btn text-white px-5 mx-5 site"  style="background-color: #5166A0">web Site</button>
            <button class="btn text-white px-5 mx-5 info" style="background-color: #5166A0"> More info</button>
           </div>
        </div>
    </div>
    <div class=" row find  mx-3 my-3" style="background-color: #E8E9E9">
        <div class=" col-md-2">
            <img src="img/img8.png" class="rounded img" alt="Cinque Terre">
        </div>
        <div class="col-md-10">
            <h2>agaWeb</h2>
            <h5>Hadj Lahbib cité les Amicales - Agadir - Maroc</h5>
            <p>Agence de communication et de publicité, design graphique,adeaux d'entreprises, publicité par objet.</p>
           <div class="px-5">
            <button class="btn  px-5 mx-5 msg text-white" style="background-color: #5166A0">send message</button>
            <button class="btn  px-5 mx-5 site text-white" style="background-color: #5166A0" >web Site</button>
            <a href=""></a><button class="btn  px-5 mx-5 text-white info" style="background-color: #5166A0"> More info</button>
           </div>
        </div>
    </div>
    
    </div>
   <div class=" row p-4  annonce">
    <div class="card mx-3 pep" style="width: 18rem;">
        <img src="img/img3.jfif" class="card-img-top mt-4"" alt="...">
        <div class="card-body">
          <h5 class="card-title">Card title</h5>
          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <a href="#" class=" " style="color: #B2CA34">see more</a>
        </div>
      </div>

      <div class="card mx-3 pepe" style="width: 18rem;">
        <img src="img/img7.jpg" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">Card title</h5>
          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <a href="#" class="" style="color: #B2CA34">see more</a>
        </div>
      </div>

      <div class="card mx-3 pepi" style="width: 18rem;">
        <img src="img/img5.png" class="card-img-top mt-4""  alt="...">
        <div class="card-body">
          <h5 class="card-title">Card title</h5>
          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <a href="#" class="" style="color: #B2CA34">see more</a>
        </div>
      </div>

      <div class="card mx-3 pepa " style="width: 18rem;">
  <img src="img/img6.jpg" class="card-img-top my-4"" alt="...">
  <div class="card-body">
    <h5 class="card-title">Card title</h5>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    <a href="#" class="" style="color: #B2CA34">see more</a>
  </div>
</div>
     
   </div>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
   <script>
            $(document).ready(function(){
                $(".pep").mouseenter(function(){
                        $(".pep").animate({
                                opacity: '2',
                                height: '400px',
                                width: '300px'
                                }),$(".pep").addClass('important');
                    });
                });
            $(document).ready(function(){
                $(".pep").mouseleave(function(){
                    $(".pep").animate({
      
                            opacity: '2',
                            height: '370',
                            width: '290px'
                            }),$(".pep").removeClass('important');;
                                            
                    });
                });

                
            
            $(document).ready(function(){
                $(".pepa").mouseenter(function(){
                        $(".pepa").animate({
                                opacity: '2',
                                height: '400px',
                                width: '300px'
                                }),$(".pepa").addClass('important');
                    });
                });
            $(document).ready(function(){
                $(".pepa").mouseleave(function(){
                    $(".pepa").animate({
      
                            opacity: '2',
                            height: '370',
                            width: '290px'
                            }),$(".pepa").removeClass('important');;
                                            
                    });
                });

                
            
            $(document).ready(function(){
                $(".pepi").mouseenter(function(){
                        $(".pepi").animate({
                                opacity: '2',
                                height: '400px',
                                width: '300px'
                                }),$(".pepi").addClass('important');
                    });
                });
            $(document).ready(function(){
                $(".pepi").mouseleave(function(){
                    $(".pepi").animate({
      
                            opacity: '2',
                            height: '370',
                            width: '290px'
                            }),$(".pepi").removeClass('important');;
                                            
                    });
                });

                
            
            $(document).ready(function(){
                $(".pepe").mouseenter(function(){
                        $(".pepe").animate({
                                opacity: '2',
                                height: '400px',
                                width: '300px'
                                }),$(".pepe").addClass('important');
                    });
                });
            $(document).ready(function(){
                $(".pepe").mouseleave(function(){
                    $(".pepe").animate({
      
                            opacity: '2',
                            height: '370',
                            width: '290px'
                            }),$(".pepe").removeClass('important');;
                                            
                    });
                });

                
            
                $(document).ready(function(){
                      $(".ville").click(function(){
                        $(".select").toggle();
                      });
                    });
                $(document).ready(function(){
                  $(".rc").click(function(){
                          $(".select").hide();
                        });
                    });
                $(document).ready(function(){
                  $(".ice").click(function(){
                          $(".select").hide();
                        });
                    });
                $(document).ready(function(){
                  $(".activité").click(function(){
                          $(".select").hide();
                        });
                    });
                    $(document).ready(function(){
                      $(".baw").click(function(){
                        $(".aga").toggle();
                      });
                    });
                    
                  
                    
                
                
   </script>
@endsection