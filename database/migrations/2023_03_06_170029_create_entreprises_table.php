<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('entreprises', function (Blueprint $table) {
            $table->string('Taxe professionnelle');
            $table->string('Raison sociale');
            $table->string('Abréviation');
            $table->string('Adresse');
            $table->string('Téléphone');
            $table->string('Faxe');
            $table->string('Email');
            $table->text('Details activite');
            $table->string('RC')->primary();
            $table->string('ICE');
            $table->string('img');
            $table->string('flyer');
            $table->date('dateCreation');
            $table->float('Capital');
            $table->string('etrenger');
            $table->string('nationalité');
            $table->string('franchises');
            $table->string('labels_certification');
            $table->foreignId('province')->references('idprovince')->on('provinces');
            $table->foreignId('form_juridique')->references('id_juridique')->on('form_juridiques');
            $table->foreignId('id_marque')->references('id_marque')->on('marques');
            $table->foreignId('persons')->references('id_person')->on('persons');
            $table->string('chiffre');
            $table->string('effectif');
            $table->string('pubiliceter');
            $table->foreignId('ville')->references('id_ville')->on('villes');
            $table->foreignId('zone')->references('id_zone')->on('zones');
            $table->foreignId('activité principale')->references('id_activité')->on('activites');
            $table->foreignId('paysExp')->references('idpays_exportation')->on('pays_exportaions');
            $table->foreignId('paysImp')->references('id_pays_importation')->on('pays_importations');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('entreprises');
    }
};
