<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('persons', function (Blueprint $table) {
            $table->id('id_person');
            $table->string('name');
            $table->string('email');
            $table->integer('GSM');
            $table->string('facebook');
            $table->string('linkedIn');
            $table->foreignId('fonction')->references('idfonction')->on('fonctions');
            
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('persons');
    }
};
